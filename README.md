This is a "Hello World" for gitlab.

The YAML config file executes a simple pipeline that will

1) build the application
2) test the application, by running it, but only for tagged versions of the code
3) build a Docker container for it and upload it to the Gitlab Container Registry